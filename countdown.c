///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down towards a significant date
//
// Example:
//   $ ./countdown
//   Reference Time: Sat May 13 00:00:00 AM HST 2023
//   Years: 1  Days: 80  Hours: 9  Minutes: 49  Seconds: 3
//   Years: 1  Days: 80  Hours: 9  Minutes: 49  Seconds: 2
//   Years: 1  Days: 80  Hours: 9  Minutes: 49  Seconds: 1
//   Years: 1  Days: 80  Hours: 9  Minutes: 49  Seconds: 0
//   Years: 1  Days: 80  Hours: 9  Minutes: 48  Seconds: 59
//
// @author Arthur Lee <leea3@hawaii.edu>
// @date   24 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <math.h>

int main() {
   
   //choose reference time (graduation date)
   const int chooseYear = 2023;
   const int chooseMonth = 5;
   const int chooseDay = 13;
   const int chooseHour = 0;
   const int chooseMinute = 0;
   const int chooseSeconds = 0;

   char buffer[50];
   struct tm time_ref;

   time_ref.tm_year = chooseYear - 1900;
   time_ref.tm_mon = chooseMonth - 1;
   time_ref.tm_mday = chooseDay;
   time_ref.tm_hour = chooseHour;
   time_ref.tm_min = chooseMinute;
   time_ref.tm_sec = chooseSeconds;
   time_ref.tm_isdst = -1; //daylight savings time

   time_t refTimeInSeconds = mktime(&time_ref);
   time_t currentTimeInSeconds = time(0);

   strftime(buffer, 50, "%a %b %d %H:%M:%S %p %Z %Y", localtime(&refTimeInSeconds));
   printf("Reference Time: %s\n", buffer);

   time_t timeDiff = refTimeInSeconds - currentTimeInSeconds;

   while(1){
      if(timeDiff <= 0){
         printf("Congrats on your graduation!\n");
         return 0;
      }

      int dispYears = floor(timeDiff / (60 * 60 * 24 * 365));
      int dispDays = floor(timeDiff / (60 * 60 * 24)) - (dispYears * 365);
      int dispHours = floor(timeDiff / (60 * 60)) - (dispYears * 365 * 24) - (dispDays * 24);
      int dispMinutes = floor(timeDiff / (60)) - (dispYears * 365 * 24 * 60) - (dispDays * 24 * 60) - (dispHours * 60);
      int dispSeconds = floor(timeDiff) - (dispYears * 365 * 24 * 60 * 60) - (dispDays * 24 * 60 * 60) - (dispHours * 60 * 60) - (dispMinutes * 60);

      printf("Years: %d  Days: %d  Hours: %d  Minutes: %d  Seconds: %d\n", dispYears, dispDays, dispHours, dispMinutes, dispSeconds);
      
      timeDiff--;

      sleep(1); //one sec delay between outputs

   }
   return 0;
}
